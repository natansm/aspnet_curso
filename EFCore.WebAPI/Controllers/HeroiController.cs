﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace EFCore.WebAPI.Controllers
{
	[Route("api/heroi")]
	[ApiController]
	public class HeroiController : ControllerBase
	{
		// GET api/values
		[HttpGet]
		public ActionResult<IEnumerable<Models.Heroi>> Get()
		{
			using (Data.HeroiContext heroiContext = new Data.HeroiContext())
            {
                return heroiContext.Herois.ToList();
            }
		}

		// GET api/values/5
		[HttpGet("{id}")]
		public ActionResult<string> Get(int id)
		{
			return "value";
		}

		// POST api/values
		[HttpPost]
		public async Task<HttpResponseMessage> Post([FromBody] Models.Heroi heroi)
		{
            try
            {
                using (Data.HeroiContext heroiContext = new Data.HeroiContext())
                {
                    heroiContext.Herois.Add(heroi);
                    await heroiContext.SaveChangesAsync();
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
            } catch (Exception)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
		}

		// PUT api/values/5
		[HttpPut("{id}")]
		public void Put(int id, [FromBody] string value)
		{
		}

		// DELETE api/values/5
		[HttpDelete("{id}")]
		public void Delete(int id)
		{
		}
	}

    internal class HttpStatusCodeResult
    {
        public HttpStatusCodeResult()
        {
        }
    }
}
